package testCases.PrivatePart;

import com.telerikacademy.finalproject.pages.EditProfilePage;
import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.UsersPage;
import org.junit.Before;
import org.junit.Test;
import testCases.BaseTest;

public class ProfileUpdateTests extends BaseTest {

    private final EditProfilePage editProfilePage = new EditProfilePage();
    private final LogInPage logInPage = new LogInPage();
    private static final String username = "savinamarinova5@gmail.com";
    private static final String password = "aaa123AAA@";

    @Before
    public void preconditions() {
        logInPage.navigateToPage();
        logInPage.logIn(username, password);
        editProfilePage.navigateToPage();
    }

    @Test
    public void TC16_SuccessfullyUpdateUserByLastName() {
        editProfilePage.updateProfileByLastName();
        actions.waitMillis(1000);

    }

    @Test
    public void TC18_SuccessfulPictureVisibilityUpdated() {
        editProfilePage.editProfilePictureVisibility();
        actions.waitMillis(1000);
        editProfilePage.assertVisibilitySuccessfullyChanged();

    }

}

