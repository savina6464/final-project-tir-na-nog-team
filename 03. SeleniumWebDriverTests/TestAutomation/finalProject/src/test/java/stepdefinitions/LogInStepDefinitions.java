package stepdefinitions;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.utils.UserActions;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class LogInStepDefinitions extends BaseStepDefinitions {
    UserActions actions = new UserActions();
    private LogInPage logInPage = new LogInPage();

    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    public void clickElement(String element) {
        actions.clickElement(element);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    public void typeInField(String value, String field) {
        actions.typeValueInField(value, field);
    }

    @Given("Wrong password message")
    @When("Wrong password message")
    @Then("Wrong password message")
    public void unsuccessfulLogIn() {
        logInPage.assertWrongPasswordMessage("errorMessage");
        actions.waitMillis(1000);
    }

    @Given("Page navigated")
    @When("Page navigated")
    @Then("Page navigated")
    public void successfulLogIn() {
        logInPage.assertPageNavigated();
        actions.waitMillis(1000);
    }
}