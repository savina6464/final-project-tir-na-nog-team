package testCases.PrivatePart;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.LogOutPage;
import com.telerikacademy.finalproject.pages.PostsPage;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import testCases.BaseTest;

public class NewPostTests extends BaseTest {
    private static final PostsPage postsPage = new PostsPage();
    private static final LogInPage logInPage = new LogInPage();
    private static final LogOutPage logOutPage = new LogOutPage();


    @BeforeClass
    public static void testClassInit() {
        logInPage.navigateToPage();
        logInPage.logIn(UserActions.getTextWithKey("username"),
                UserActions.getTextWithKey("password"));
    }

    @Before
    public void testInit() {
        postsPage.navigateToPage();
    }

    @AfterClass
    public static void testClassTearDown() {
        logOutPage.logOut();
    }

    @Test
    public void TC23_SuccessfullyCreateNewPost() {
        postsPage.createNewPost();
        actions.waitMillis(1000);
        postsPage.assertPostExists("postTitle");


    }

}
