package testCases.PublicPart;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.LogOutPage;
import com.telerikacademy.finalproject.pages.PostsPage;
import com.telerikacademy.finalproject.pages.UsersPage;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import testCases.BaseTest;

public class SearchUsersTests extends BaseTest {
    private final UsersPage usersPage = new UsersPage();
    private final PostsPage postsPage = new PostsPage();
    private static final String searchedName = "Silvia";
    private static final String email = "user5555@abv.bg";
    private static final LogOutPage logOutPage = new LogOutPage();


    @Before
    public void preconditions() {
        usersPage.navigateToPage();
    }

    @AfterClass
    public static void testClassTearDown() {
        logOutPage.logOut();
    }

    @Test
    public void TC35_SearchUserByName() {
        usersPage.searchByName(searchedName);
        actions.waitMillis(1000);
        usersPage.assertNamePresent();

    }

    @Test
    public void TC34_SearchUserByEmail() {
        usersPage.searchByEmail(email);
        actions.waitMillis(1000);
        usersPage.assertEmailPresent();



    }
}
