package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;

public class PostsPage extends BasePage {
    public PostsPage() {
        super("posts.url");
    }

    public void openAllPost() {
        actions.moveToElement("latest.Posts");
        actions.clickElement("latest.Posts");
        actions.waitMillis(1000);
    }

    public void sortPostsByComments() {
        actions.clickElement("comment.Button");
        actions.waitForElementVisible("posts.area", 30);
    }

    public void sortPostsByLikes() {
        actions.clickElement("likePost.Button");
        actions.waitForElementVisible("posts.area", 30);
    }

    public void createNewPost() {
        actions.waitForElementVisible("latest.Posts", 50);
        actions.moveToElement("latest.Posts");
        actions.waitForElementVisible("new.Post", 50);
        actions.clickElement("new.Post");
        actions.waitForElementVisible("title.Field", 50);
        actions.typeValueInField("New Selenium Test", "title.Field");
        actions.clickElement("savePost.Button");
        actions.waitMillis(1000);

    }

    public void searchPostByTitle(String postTitle) {
        actions.waitForElementVisible("search.Post", 30);
        actions.typeValueInField(postTitle, "search.Post");
        actions.waitMillis(1000);
        actions.clickElement("searchPost.Button");
        actions.waitMillis(1000);
    }

    public void assertPostExists(String postTitle) {
        navigateToPage();
        searchPostByTitle(UserActions.getTextWithKey("editPostTitle"));
        actions.waitMillis(1000);
    }

    public void assertPostPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void openPost() {
        actions.waitForElementVisible("postTitle.Find", 30);
        actions.clickElement("postTitle.Find");
    }

    public void likePost() {
        actions.waitForElementVisible("postLike.Button", 30);
        actions.clickElement("postLike.Button");
        actions.waitMillis(500);

    }
}

