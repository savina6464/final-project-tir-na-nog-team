package testCases.PrivatePart;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.LogOutPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import org.junit.Before;
import org.junit.Test;
import testCases.BaseTest;


public class LogInLogOutTests extends BaseTest {
    private static final String username = "savinamarinova5@gmail.com";
    private static final String password = "aaa123AAA@";
    private static final String invalidPassword = "aaa123AAA#";
    private static final String errorMessage = "Wrong username or password.";
    private final NavigationPage navigationPage = new NavigationPage();
    private final LogInPage logInPage = new LogInPage();
    private final LogOutPage logOutPage = new LogOutPage();

    @Before
    public void preconditions() {
        logInPage.navigateToPage();
    }

    @Test
    public void TC13_UnsuccessfulLogInWithInvalidPassword() {
        logInPage.logIn(username, invalidPassword);
        logInPage.assertWrongPasswordMessage(errorMessage);
        actions.waitMillis(1000);
    }

    @Test
    public void TC12_SuccessfulLogInWithValidCredentials() {
        logInPage.logIn(username, password);
        navigationPage.assertPageNavigated();
        actions.waitMillis(1000);

    }

    @Test
    public void TC15_SuccessfulLogOut() {
        logInPage.logIn(username, password);
        logOutPage.logOut();
        navigationPage.assertPageNavigated();
        actions.waitMillis(1000);

    }
}