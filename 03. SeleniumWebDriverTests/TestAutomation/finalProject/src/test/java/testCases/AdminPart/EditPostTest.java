package testCases.AdminPart;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.*;
import testCases.BaseTest;

public class EditPostTest extends BaseTest {
    private static final LogInPage logInPage = new LogInPage();
    private static final LogOutPage logOutPage = new LogOutPage();
    private final EditPostPage editPostsPage = new EditPostPage();
    private final PostsPage postsPage = new PostsPage();


    @BeforeClass
    public static void testClassInit() {
        logInPage.navigateToPage();
        logInPage.logIn(UserActions.getTextWithKey("usernameAdmin"),
                UserActions.getTextWithKey("passwordAdmin"));
    }

    @Before
    public void testInit() {
        editPostsPage.navigateToPage();
    }

    @AfterClass
    public static void testClassTearDown() {
        logOutPage.logOut();
    }

    @Test
    public void TC38_UpdatePost() {
        editPostsPage.updatePost("editPostTitle");


    }

}
