package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;

public class LogInPage extends BasePage {
    public LogInPage() {
        super("login.url");
    }

    public void logIn(String username, String password) {
        actions.waitForElementVisible("login.UserNameField", 20);
        actions.typeValueInField(username, "login.UserNameField");
        actions.waitMillis(500);
        actions.waitForElementVisible("login.PasswordField", 20);
        actions.typeValueInField(password, "login.PasswordField");
        actions.waitMillis(500);
        actions.clickElement("login.LoginButton");
        actions.waitMillis(500);

    }

    public void assertWrongPasswordMessage(String text) {
        Assert.assertTrue("Wrong username or password.", driver.getPageSource().contains(text));
    }
}