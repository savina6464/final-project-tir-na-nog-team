package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.Assert;

public class EditPostPage extends BasePage {

    public EditPostPage() {
        super("editPost.url");
    }

    public void updatePost(String updatedPost) {
        assertPageNavigated();
        actions.waitForElementVisible("edit.Title", 30);
        actions.clearField("edit.Title");
        actions.typeValueInField(UserActions.getTextWithKey( "editPostTitle"), "edit.Title");
        actions.clickElement("savePost.Button");
    }
}
