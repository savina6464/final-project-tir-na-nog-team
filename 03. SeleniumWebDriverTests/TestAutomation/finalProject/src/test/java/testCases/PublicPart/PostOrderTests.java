package testCases.PublicPart;

import com.telerikacademy.finalproject.pages.BasePage;
import com.telerikacademy.finalproject.pages.PostsPage;
import com.telerikacademy.finalproject.pages.UsersPage;
import org.junit.Before;
import org.junit.Test;
import testCases.BaseTest;

public class PostOrderTests extends BaseTest {
    private PostsPage postsPage = new PostsPage();

    @Before
    public void preconditions() {
        postsPage.navigateToPage();
    }

    @Test
    public void TC45_SortUsersPostsByComments() {
        postsPage.sortPostsByComments();
        actions.waitMillis(30);
        postsPage.assertPageNavigated();
        actions.waitMillis(30);
    }

    @Test
    public void TC45_SortUsersPostsByLikes() {
        postsPage.sortPostsByLikes();
        actions.waitMillis(30);
        postsPage.assertPageNavigated();
        actions.waitMillis(30);
    }
}
