package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditProfilePage extends BasePage {

    public EditProfilePage() {
        super("profileEdit.url");
    }

    private final UsersPage usersPage = new UsersPage();
    public void updateProfileByLastName() {
        actions.waitForElementVisible("lastName.Field", 30);
        actions.clearField("lastName.Field");
        actions.typeValueInFieldByConfProperty("updatedProfileLastName", "lastName.Field");
        actions.waitMillis(1000);
        actions.clickElement("save.Button");
        actions.waitMillis(1000);
    }

    public void editProfilePictureVisibility() {
        actions.waitForElementVisible("visibility.Profile", 30);
        actions.clickElement("visibility.Profile");
        actions.clickElement("visibility.Connections");


    }

    public void assertVisibilitySuccessfullyChanged() {
        assertPageNavigated();
        actions.waitForElementVisible("visibility.Connections", 30);
        actions.assertElementPresent("visibility.Connections");
        actions.clickElement("saveChanges.Button");
    }

    public void assertLastNameChanged() {
        usersPage.navigateToPage();
        usersPage.searchByName("updatedProfileLastName");
    }
}

