package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;

public class UsersPage extends BasePage {
    public UsersPage() {
        super("users.url");
    }

    public void searchByName(String name) {
        actions.waitForElementVisible("search.Field", 20);
        actions.typeValueInField(name, "search.Field");
        actions.clickElement("search.Button");
        actions.waitMillis(1000);
        actions.clickElement("searched.Name");
    }

    public void searchByEmail(String email) {
        actions.waitForElementVisible("search.Field", 20);
        actions.typeValueInField(email, "search.Field");
        actions.clickElement("search.Button");
        actions.waitMillis(1000);
        actions.clickElement("searched.Name");

    }

    public void assertNamePresent() {
        navigateToPage();
        searchByName("Silvia");
        assertNamePresent();
    }

    public void assertEmailPresent() {
        navigateToPage();
        searchByEmail("user5555@abv.bg");
        actions.clickElement("searched.Name");
    }


}